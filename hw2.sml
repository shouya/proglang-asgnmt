(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw

exception IllegalMove



(* put your solutions for problem 2 here *)

(* problem 1 *)
fun all_except_option (str : string, lst : string list) =
    let fun helper (str, lst) =
            case lst of
                []    => []
              | x::xs => if same_string(x, str)
                         then xs
                         else x :: helper(str, xs);
        val rst = helper(str, lst)
    in if rst = lst then NONE else SOME rst
    end

type subst_t = string list list

fun get_substitutions1 (subst : subst_t, str : string) =
    case subst of
        []   => []
      | x::xs => (case all_except_option (str, x) of
                      NONE   => []
                    | SOME v => v) @
                get_substitutions1 (xs, str)

(* ????? isn't this function same as `get_substitutions1`? *)
fun get_substitutions2 (subst : subst_t, str : string) =
    let
        val helper = get_substitutions1
    in
        helper (subst, str)
    end


type name_t = {first: string, middle: string, last: string}

fun similar_names (subst : subst_t, name : name_t) =
    let fun construct_name (first_name, {first=f, middle=m, last=l}) =
            {first=first_name, middle=m, last=l};
        fun combine (name_list : string list, name : name_t) =
            case name_list of
                []    => []
              | x::xs => construct_name(x, name) :: combine (xs, name)
    in name :: combine (get_substitutions1 (subst, #first name), name)
    end


(* problem 2 *)
fun card_color ((suit, _)) = case suit of
                                 Spades   => Black
                               | Clubs    => Black
                               | Hearts   => Red
                               | Diamonds => Red

fun card_value ((_, value)) = case value of
                                  Ace   => 11
                                | Jack  => 10
                                | Queen => 10
                                | King  => 10
                                | Num n => n

fun remove_card (cs : card list, c : card, e : exn) =
    let fun helper (cs, c) =
            case cs of
                []    => []
              | x::xs => if x = c then xs else x :: helper (xs, c);
        val result = helper (cs, c)
    in if result = cs then raise e else result
    end

fun all_same_color (cs : card list) =
    case cs of
         []   => true
      | x::[] => true
      | x::y::xs => (card_color x) = (card_color y) andalso
                    all_same_color (y::xs)

fun sum_cards (cs : card list) =
    case cs of
        []   => 0
     | x::xs => (card_value x) + sum_cards xs


fun score (cs : card list, goal : int) =
    let val sum = sum_cards cs;
        val preliminary_score = if sum > goal
                                then 3 * (sum - goal)
                                else goal - sum
    in preliminary_score div (if all_same_color cs then 2 else 1)
    end

fun officiate (cs : card list, moves : move list, goal : int) =
    let fun step (cs : card list, hc : card list, moves : move list) =
            case moves of
                Draw      :: xs => let val (c::cs) = cs in (cs, c :: hc, xs) end
              | Discard c :: xs => (cs, remove_card (hc, c, IllegalMove), xs)
        fun recur (cs, hc, moves) =
            case moves of
                     []    => hc
              | Draw :: xs => if null cs then hc
                              else let val rst = (step (cs, hc, moves))
                                   in  if sum_cards (#2 rst) > goal then (#2 rst)
                                       else recur rst
                                   end
              | _          => recur (step (cs, hc, moves))
    in score (recur (cs, [], moves), goal)
    end
