# -*- coding: utf-8 -*-
# University of Washington, Programming Languages, Homework 6, hw6runner.rb

# This is the only file you turn in, so do not modify the other files as
# part of your solution.

class MyPiece < Piece
  # The constant All_My_Pieces should be declared here
  All_My_Pieces = [rotations([[0, 0], [1, 0], [0, 1], [1, 1], [0, 2]]), # o_
                   [[[0, 0], [-1, 0], [1, 0], [2, 0], [-2, 0]],
                    [[0, 0], [0, -1], [0, 1], [0, 2], [0, -2]]],  # long I
                   rotations([[0, 0], [1, 0], [0, 1]]),
                  ].concat(All_Pieces)

  # your enhancements here
  def self.next_piece(board)
    Piece.new(All_My_Pieces.sample, board)
  end

end

class MyBoard < Board
  # your enhancements here
  attr_accessor :cheated

  def initialize(*)
    super

    @cheated = false
  end

  def next_piece
    @current_block = MyPiece.next_piece(self)
    if @cheated
      @current_block = Piece.new([[[0,0]]], self)
      @cheated = false
    end
    @current_pos = nil
  end

  def store_current
    locations = @current_block.current_rotation
    displacement = @current_block.position
    locations.each_with_index{|_,index|
      current = locations[index];
      @grid[current[1]+displacement[1]][current[0]+displacement[0]] =
      @current_pos[index]
    }
    remove_filled
    @delay = [@delay - 2, 80].max
  end

  def score=(score)
    @score = score
  end
end

class MyTetris < Tetris
  # your enhancements here

  def set_board
    @canvas = TetrisCanvas.new
    @board = MyBoard.new(self)
    @canvas.place(@board.block_size * @board.num_rows + 3,
                  @board.block_size * @board.num_columns + 6, 24, 80)
    @board.draw
  end

  def key_bindings
    super

    @root.bind('u', proc {@board.rotate_counter_clockwise;
                 @board.rotate_counter_clockwise})
    @root.bind('c', proc { cheat })
  end

  def cheat
    return if @board.cheated or @board.score < 100
    @board.cheated = true
    @board.score -= 100
    update_score
  end
end


