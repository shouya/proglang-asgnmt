(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let
	val r = g f1 f2
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)

fun only_capitals (xs : string list) =
    List.filter (fn x => Char.isUpper (String.sub (x,0))) xs

fun longest_string1 (xs : string list) =
    let fun longer (str1, str2) =
            if String.size str1 > String.size str2 then str1 else str2
    in
        case xs of
            [] => ""
          | _  => List.foldl longer "" xs
    end

fun longest_string2 (xs : string list) =
    let fun longer (str1, str2) =
            if String.size str1 >= String.size str2 then str1 else str2
    in
        case xs of
            [] => ""
          | _  => List.foldl longer "" xs
    end

(* improved re-usability *)
fun longest_string2' (xs : string list) =
    longest_string1 (List.rev xs)

fun longest_string_helper (cmp : int * int -> bool) xs =
    let fun longer (str1, str2) =
            if cmp (String.size str1, String.size str2) then str1 else str2
    in
        case xs of
            [] => ""
          | _  => List.foldl longer "" xs
    end

val longest_string3 = longest_string_helper (fn (a,b) => a >  b)
val longest_string4 = longest_string_helper (fn (a,b) => a >= b)

val longest_capitalized = longest_string1 o only_capitals
val rev_string = String.implode o List.rev o String.explode

fun first_answer trans xs =
    if null xs then raise NoAnswer
    else let val rst = trans (hd xs) in
             case rst of
                 NONE   => first_answer trans (tl xs)
               | SOME x => x end

fun all_answers trans xs =
    if null xs then SOME []
    else let val rst = foldl (fn (a,b) =>
                                 let val rst = trans a in
                                     case rst of
                                         NONE   => b
                                       | SOME x => valOf rst @ b end) [] xs
         in if null rst then NONE else SOME rst
         end

val count_wildcards = g (fn () => 1) (fn _ => 0)
val count_wild_and_variable_lengths = g (fn () => 1) (fn x => String.size x)
fun count_some_var (name, pat) =
    g (fn _ => 0) (fn x => if x = name then 1 else 0) pat

fun check_pat pat =
    let fun list_vars pat =
            case pat of
                Variable x => [x]
              | TupleP ps  => List.foldl (fn (p,i) => (list_vars p) @ i) [] ps
	      | ConstructorP(_,p) => list_vars p
              | _ => []
        fun exists' xs x = List.exists (fn y => x = y) xs
        fun check_var strs pat =
            case pat of
                Variable x => not (exists' strs x)
              | TupleP ps  => List.all (fn p => check_var strs p) ps
              | ConstructorP(_,p) => check_var strs p
              | _ => true
    in check_var (list_vars pat) pat
    end

fun match (v : valu, p : pattern) =
    case p of
	Wildcard          => SOME []
      | Variable x        => SOME [(x, v)]
      | UnitP             => (case v of Unit    => SOME [] | _ => NONE)
      | ConstP _          => (case v of Const _ => SOME [] | _ => NONE)
      | TupleP ps         => (case v of Tuple vs =>
                                        all_answers match (ListPair.zip (vs,ps))
                                      | _ => NONE)
      | ConstructorP(n,p) => (case v of Constructor (m,v) => if m = n
                                                             then match (v,p)
                                                             else NONE
                                      | _                 => NONE)


fun first_match v ps =
    SOME (first_answer (fn p => match (v,p)) ps) handle NoAnswer => NONE

