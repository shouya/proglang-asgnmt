fun is_older (date1 : int * int * int, date2 : int * int * int) =
    if #1 date1 < #1 date2 orelse
       #1 date1 = #1 date2 andalso #2 date1 < #2 date2 orelse
       #1 date1 = #1 date2 andalso #2 date1 = #2 date2 andalso #3 date1 < #3 date2
    then true
    else false

fun number_in_month (dates : (int * int * int) list, month : int) =
    if null dates then 0
    else if #2 (hd dates) = month
    then 1 + number_in_month (tl dates, month)
    else 0 + number_in_month (tl dates, month)

fun number_in_months (dates : (int * int * int) list, months : int list) =
    if null months
    then 0
    else (number_in_month  (dates, hd months)) +
         (number_in_months (dates, tl months))

fun dates_in_month (dates : (int * int * int) list, month : int) =
    if null dates then []
    else if #2 (hd dates) = month
    then (hd dates) :: dates_in_month (tl dates, month)
    else               dates_in_month (tl dates, month)

fun dates_in_months (dates : (int * int * int) list, months : int list) =
    if null months
    then []
    else (dates_in_month  (dates, hd months)) @
         (dates_in_months (dates, tl months))

fun get_nth (strings : string list, n : int) =
    if n - 1 = 0
    then hd strings
    else get_nth (tl strings, n - 1)

fun date_to_string (date : int * int * int) =
    let fun month_to_string (month : int) =
            if      month = 1 then "January"
            else if month = 2 then "February"
            else if month = 3 then "March"
            else if month = 4 then "April"
            else if month = 5 then "May"
            else if month = 6 then "June"
            else if month = 7 then "July"
            else if month = 8 then "August"
            else if month = 9 then "September"
            else if month = 10 then "October"
            else if month = 11 then "November"
            else if month = 12 then "December"
            else                    "Unknown"
    in
        (month_to_string (#2 date)) ^ " " ^
        (Int.toString (#3 date)) ^ ", " ^
        (Int.toString (#1 date))
    end


fun number_before_reaching_sum (sum : int, xs : int list) =
    if sum <= 0 then ~1
    else 1 + (number_before_reaching_sum (sum - hd xs, tl xs))

val month_day = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

fun what_month (day : int) =
    number_before_reaching_sum (day, month_day) + 1

fun month_range (from : int, to :int) =
    if from = to
    then (what_month from) :: []
    else (what_month from) :: month_range (from + 1, to)


fun oldest (dates : (int * int * int) list) =
    if null dates then NONE
    else let fun oldest_helper (dates : (int * int * int) list) =
                 if null (tl dates) then hd dates
                 else let val old = oldest_helper (tl dates)
                      in
                          if is_older (hd dates, old)
                          then hd dates
                          else old
                      end
         in SOME (oldest_helper (dates))
         end


fun remove_duplication (months : int list) =
    if null months then []
    else
        let fun list_contain_q (xs : int list, x : int) =
                if null xs then false
                else if x = hd xs then true
                else list_contain_q (tl xs, x);
            val rest = remove_duplication (tl months)
        in
            if list_contain_q (rest, (hd months))
            then rest
            else hd months :: rest
        end


fun number_in_months_challenge (dates : (int * int * int) list,
                                months : int list) =
    number_in_months (dates, remove_duplication months)

fun dates_in_months_challenge (dates : (int * int * int) list,
                               months : int list) =
    dates_in_months (dates, remove_duplication months)

fun reasonable_date (date : int * int * int) =
    let val year  = #1 date;
        val month = #2 date;
        val day   = #3 date;
    in
        if year <= 0 then false
        else if month > 12 orelse month < 1 then false
        else if day < 0 then false
        else if month <> 2 andalso
                day > List.nth (month_day, month - 1) then false
        else if month = 2 then
                let val leap_year_q = year mod 400 = 0 orelse
                                      (year mod 4 = 0 andalso
                                       year mod 100 <> 0)
                in
                    if leap_year_q andalso day > 29 then false
                    else if day > 28                then false
                    else                                 true
                end
        else true
    end


